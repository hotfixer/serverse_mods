using System;
using System.Collections.Generic;
using System.Linq;
using VRage.Game.Components;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Definitions;
using Sandbox.Game;
using Sandbox.ModAPI;
using VRage.Game;
using VRage.ObjectBuilders;
using VRage.ModAPI;
using VRage.Game.ModAPI;
using VRageMath;
using Sandbox.Game.Entities;
using Sandbox.Game.Weapons;
using VRage.Game.Entity;
using VRage.Utils;
using VRage;

/*  
  Welcome to Modding API. This is second of two sample scripts that you can modify for your needs,
  in this case simple script is prepared that will alter behaviour of sensor block
  This type of scripts will be executed automatically  when sensor (or your defined) block is added to world
 */
namespace Phoera.GringProgression
{
  [MySessionComponentDescriptor(MyUpdateOrder.BeforeSimulation)]
  public class Core : CoreBase
  {
    NetworkHandlerSystem nhs = new NetworkHandlerSystem(3);
    Settings settings = new Settings();
    Dictionary<long, HashSet<MyDefinitionId>> playersData = new Dictionary<long, HashSet<MyDefinitionId>>();

    Dictionary<MyDefinitionId, HashSet<MyDefinitionId>> variantGroups =
      new Dictionary<MyDefinitionId, HashSet<MyDefinitionId>>(MyDefinitionId.Comparer);

    Dictionary<long, ulong> userIds = new Dictionary<long, ulong>();

    const string configFile = "config.xml";
    const string playerFile = "{0}.xml";
    private MessageEventCaller<long> PlayerInit;
    private bool isInitialized = false;

    public MessageEventCaller<MyDefinitionId> SendUnlockNotification { get; private set; }

    public override void Deinitialize()
    {
      try
      {
        MyAPIGateway.Session.Player.Controller.ControlledEntityChanged -= Controller_ControlledEntityChanged;
        nhs.Unload();
      }
      catch (Exception e)
      {
      }

    }

    public override bool Initialize(out MyUpdateOrder order)
    {
      order = MyUpdateOrder.NoUpdate;
      if (NetworkHandlerSystem.IsClient)
      {
        if (MyAPIGateway.Session.Player == null)
        {
          return false;
        }
      }
      if (NetworkHandlerSystem.IsServer)
      {
        try
        {
          using (var sw = MyAPIGateway.Utilities.ReadFileInWorldStorage(configFile, typeof(Core)))
            settings = MyAPIGateway.Utilities.SerializeFromXML<Settings>(sw.ReadToEnd());
        }
        catch (Exception e)
        {
          MyLog.Default.WriteLine($"Get World: {e.Message}");
          MyLog.Default.Flush();
        }

        if (settings.AlwaysUnlocked == null)
          settings.AlwaysUnlocked = new HashSet<SerializableDefinitionId>();
        MyAPIGateway.Session.DamageSystem.RegisterDestroyHandler(0, DestroyHandler);
        MyVisualScriptLogicProvider.PlayerResearchClearAll();
        PrepareCache();
      }

      PlayerInit = nhs.Create<long>(null, PlayerJoined, EventOptions.OnlyToServer);
      SendUnlockNotification = nhs.Create<MyDefinitionId>(LearnedById, null, EventOptions.OnlyToTarget);
      if (NetworkHandlerSystem.IsClient)
      {
        try
        {
          MyVisualScriptLogicProvider.ResearchListWhitelist(true);
          MyVisualScriptLogicProvider.PlayerResearchClear();
          MyLog.Default.WriteLine($"Check ControlledEntity");
          if (MyAPIGateway.Session.Player.Controller.ControlledEntity != null &&
              MyAPIGateway.Session.Player.Controller.ControlledEntity.Entity is IMyCharacter)
          {
            MyLog.Default.WriteLine($"InvokeOnGameThread Start");
            MyLog.Default.Flush();
            MyAPIGateway.Utilities.InvokeOnGameThread(() =>
            {
              try
              {
                MyLog.Default.WriteLine($"Start InitPlayer");
                MyLog.Default.Flush();
                PlayerInit(MyAPIGateway.Session.Player.PlayerID);
              }catch(Exception e)
              {
                MyLog.Default.WriteLine($"PlayerInit: {e.Message}");
                MyLog.Default.Flush();
              }
            });
          }
          else
          {
            MyLog.Default.WriteLine($"ControlledEntityChanged");
            MyLog.Default.Flush();
            MyAPIGateway.Session.Player.Controller.ControlledEntityChanged += Controller_ControlledEntityChanged;
          }
        }catch(Exception e)
        {
          MyLog.Default.WriteLine($"NetworkHandlerSystem INIT: {e.Message}");
          MyLog.Default.Flush();
        }
      }
      MyLog.Default.WriteLine($"SUCCESSFUL INIT!");
      MyLog.Default.Flush();
      return true;
    }

    void LearnedById(MyDefinitionId id, ulong sender)
    {
      try
      {
        MyAPIGateway.Utilities.ShowNotification(
          $"You can now build {MyDefinitionManager.Static.GetCubeBlockDefinition(id).DisplayNameText}.");
      }
      catch (Exception e)
      {
        //just in case
        MyLog.Default.WriteLine($"ERROR LearnedById: {e.Message}");
      }
    }

    private void Controller_ControlledEntityChanged(VRage.Game.ModAPI.Interfaces.IMyControllableEntity arg1,
      VRage.Game.ModAPI.Interfaces.IMyControllableEntity arg2)
    {
      if (arg2 != null && arg2.Entity is IMyCharacter)
      {
        MyAPIGateway.Utilities.InvokeOnGameThread(() =>
        {
          try
          {
            if (MyAPIGateway.Session != null && MyAPIGateway.Session.Player != null)
              PlayerInit(MyAPIGateway.Session.Player.PlayerID);
          }
          catch(Exception e)
          {
            MyLog.Default.WriteLine($"ERROR PlayerInit Entity Change: {e.Message}");
            MyLog.Default.Flush();
          }
        });
        MyAPIGateway.Session.Player.Controller.ControlledEntityChanged -= Controller_ControlledEntityChanged;
      }
    }

    void DestroyHandler(object target, MyDamageInformation damage)
    {
      if (damage.Type == MyDamageType.Grind)
      {

        if (target is IMySlimBlock)
        {
          var slim = target as IMySlimBlock;
          CalculatePlayers(slim.BlockDefinition.Id, slim.CubeGrid.GridIntegerToWorld(slim.Position), damage.AttackerId);
        }
        else if (target is IMyCubeBlock) //just in cause
        {
          var fat = target as IMyCubeBlock;
          CalculatePlayers(fat.BlockDefinition, fat.GetPosition(), damage.AttackerId);
        }
      }
    }

    void CalculatePlayers(MyDefinitionId blockId, Vector3D pos, long attackerId)
    {
      IMyEntity ent;
      if (settings.UseLearnRadius)
      {
        if (MyAPIGateway.Entities.TryGetEntityById(attackerId, out ent))
        {
          var hand = ent as IMyHandheldGunObject<MyToolBase>;
          if (hand != null && hand.DefinitionId.TypeId == typeof(MyObjectBuilder_AngleGrinder))
          {
            var players = new List<IMyPlayer>();
            MyAPIGateway.Players.GetPlayers(players);
            var pl = players.FirstOrDefault(p => p.IdentityId.Equals(hand.OwnerIdentityId));
            var faction = MyAPIGateway.Session.Factions.TryGetPlayerFaction(pl.PlayerID);
            if (faction != null)
            {
              foreach (var player in players)
              {
                var f = MyAPIGateway.Session.Factions.TryGetPlayerFaction(player.PlayerID);
                if (f != null && f.FactionId == faction.FactionId)
                {
                  UnlockById(blockId, player.PlayerID);
                }
              }
            }
          }
        }
      }

      if (MyAPIGateway.Entities.TryGetEntityById(attackerId, out ent))
      {
        var hand = ent as IMyHandheldGunObject<MyToolBase>;
        if (hand != null && hand.DefinitionId.TypeId == typeof(MyObjectBuilder_AngleGrinder))
        {
          var players = new List<IMyPlayer>();
          MyAPIGateway.Players.GetPlayers(players);
          var pl = players.FirstOrDefault(p => p.IdentityId.Equals(hand.OwnerIdentityId));
          if (pl != null)
          {
            UnlockById(blockId, pl.PlayerID);
          }
        }
      }
    }

    private void PrepareCache()
    {
      foreach (var cube in MyDefinitionManager.Static.GetAllDefinitions().OfType<MyCubeBlockDefinition>())
      {
        if (cube.BlockStages != null && cube.BlockStages.Length > 0)
        {
          var ids = new HashSet<MyDefinitionId>(cube.BlockStages, MyDefinitionId.Comparer);
          ids.Add(cube.Id);
          foreach (var id in ids)
          {
            variantGroups[id] = ids;
          }
        }
      }
    }

    public override void SaveData()
    {
      if (!Initialized)
        return;
      using (var sw = MyAPIGateway.Utilities.WriteFileInWorldStorage(configFile, typeof(Core)))
        sw.Write(MyAPIGateway.Utilities.SerializeToXML(settings));
      foreach (var player in playersData)
      {
        try
        {
          using (var sw =
            MyAPIGateway.Utilities.WriteFileInWorldStorage(string.Format(playerFile, player.Key), typeof(Core)))
            sw.Write(MyAPIGateway.Utilities.SerializeToXML(player.Value.Select(s => (SerializableDefinitionId) s)
              .ToList()));
        }
        catch (Exception e)
        {
          MyLog.Default.WriteLine($"ERROR SaveData: {e.Message}");
        }
      }
    }

    void PlayerJoined(long playerID, ulong sender)
    {
      MyLog.Default.WriteLine($"PlayerJoined THe game! {playerID}");
      MyLog.Default.Flush();
      try
      {
        if (sender == 0)
          sender = MyAPIGateway.Session?.Player?.SteamUserId ?? 0;
        userIds[playerID] = sender;
        var playerIds = (HashSet<MyDefinitionId>) null;
        try
        {
          using (var sw =
            MyAPIGateway.Utilities.ReadFileInWorldStorage(string.Format(playerFile, playerID), typeof(Core)))
          {
            var ids = MyAPIGateway.Utilities.SerializeFromXML<List<SerializableDefinitionId>>(sw.ReadToEnd());
            playerIds = new HashSet<MyDefinitionId>(ids.Select(s => (MyDefinitionId) s), MyDefinitionId.Comparer);
          }
        }
        catch (Exception e)
        {
          MyLog.Default.WriteLine($"PlayerJoinedRead: {e.Message}");
          MyLog.Default.Flush();
        }

        if (playerIds == null)
          playerIds = new HashSet<MyDefinitionId>(MyDefinitionId.Comparer);
        if (playerIds.Count == 0)
        {
          MyVisualScriptLogicProvider.ClearAllToolbarSlots(playerID);
        }

        foreach (var id in settings.AlwaysUnlocked)
        {
          playerIds.Add(id);
        }

        playersData[playerID] = playerIds;
        foreach (var id in playerIds.ToList())
        {
          UnlockById(id, playerID, true);
        }
      }
      catch (Exception e)
      {
        MyLog.Default.WriteLine($"PlayerJoined: {e.Message}");
        MyLog.Default.WriteLine($"PlayerJoinedStack: {e.StackTrace}");
        MyLog.Default.Flush();

      }
      MyLog.Default.Flush();
    }

    void UnlockById(MyDefinitionId blockId, long player, bool force = false)
    {
      try
      {
        var playerData = playersData[player];
        if (!force && playerData.Contains(blockId))
        {
          return;
        }

        var ids = new HashSet<MyDefinitionId>();
        ids.Add(blockId);
        var cb = MyDefinitionManager.Static.GetCubeBlockDefinition(blockId);

        if (!cb.Public)
          return;
        ulong steamId;

        if (!force && userIds.TryGetValue(player, out steamId))
        {
          SendUnlockNotification(blockId, steamId);
        }

        var dg = MyDefinitionManager.Static.TryGetDefinitionGroup(cb.BlockPairName);
        if (dg != null)
        {
          if (dg.Large != null)
            ids.Add(dg.Large.Id);
          if (dg.Small != null)
            ids.Add(dg.Small.Id);
        }

        if (!cb.GuiVisible || (cb.BlockStages != null && cb.BlockStages.Length > 0))
          foreach (var bid in ids.ToList())
          {
            HashSet<MyDefinitionId> blocks;
            if (variantGroups.TryGetValue(bid, out blocks))
            {
              if (blocks != null)
                foreach (var block in blocks)
                {
                  ids.Add(block);
                }
            }
          }

        foreach (var id in ids)
        {
          playerData.Add(id);
          MyVisualScriptLogicProvider.PlayerResearchUnlock(player, id);
        }

      }
      catch (Exception e)
      {
        MyLog.Default.WriteLine($"ERROR: {e.StackTrace}");
        MyLog.Default.Flush();
      }
    }
  }

  public class Settings
  {
    public float LearnRadius { get; set; } = 5;
    public bool UseLearnRadius { get; set; } = false;
    public HashSet<SerializableDefinitionId> AlwaysUnlocked = new HashSet<SerializableDefinitionId>();
  }
}