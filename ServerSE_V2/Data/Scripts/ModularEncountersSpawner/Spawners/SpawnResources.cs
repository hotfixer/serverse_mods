using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandbox.Common;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Common.ObjectBuilders.Definitions;
using Sandbox.Definitions;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.EntityComponents;
using Sandbox.Game.GameSystems;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.Entity;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using VRage.ObjectBuilders;
using VRage.Utils;
using VRageMath;
using ModularEncountersSpawner;
using ModularEncountersSpawner.Configuration;
using ModularEncountersSpawner.Templates;

namespace ModularEncountersSpawner.Spawners{

	public static class SpawnResources{
		
		public static HashSet<IMyEntity> EntityList = new HashSet<IMyEntity>();
		public static List<MySafeZone> safezoneList = new List<MySafeZone>();
		public static List<MyPlanet> PlanetList = new List<MyPlanet>();
		//public static List<IMyPlayer> PlayerList = new List<IMyPlayer>();
		
		public static Random rnd = new Random();
		
		public static void RefreshEntityLists(){
			
			EntityList.Clear();
			safezoneList.Clear();
			PlanetList.Clear();
			
			MyAPIGateway.Entities.GetEntities(EntityList);
			
			foreach(var entity in EntityList){
				
				if(entity as MySafeZone != null){
					
					safezoneList.Add(entity as MySafeZone);
					
				}
				
				if(entity as MyPlanet != null){
					
					PlanetList.Add(entity as MyPlanet);
					
				}
				
			}
	
		}
		
		public static Vector3D CreateDirectionAndTarget(Vector3D startDirCoords, Vector3D endDirCoords, Vector3D startPathCoords, double pathDistance){
	
			var direction = Vector3D.Normalize(endDirCoords - startDirCoords);
			var coords = direction * pathDistance + startPathCoords;
			return coords;
			
		}
				
		public static double GetDistanceFromSurface(Vector3D position, MyPlanet planet){
			
			if(planet == null){
				
				return 0;
				
			}
			
			var thisPosition = position;
			var surfacePoint = planet.GetClosestSurfacePointGlobal(ref thisPosition);
			return Vector3D.Distance(thisPosition, surfacePoint);
			
		}
		
		public static Vector3D GetNearestSurfacePoint(Vector3D position, MyPlanet planet){
			
			if(planet == null){
				
				return Vector3D.Zero;
				
			}
			
			var thisPosition = position;
			var surfacePoint = planet.GetClosestSurfacePointGlobal(ref thisPosition);
			return surfacePoint;
			
		}
		
		public static IMyPlayer GetNearestPlayer(Vector3D checkCoords){
			
			IMyPlayer thisPlayer = null;
			double distance = -1;
			
			foreach(var player in MES_SessionCore.PlayerList){
				
				if(player.Character == null || player.IsBot == true){
					
					continue;
					
				}
				
				var currentDist = Vector3D.Distance(player.GetPosition(), checkCoords);
				
				if(thisPlayer == null){
					
					thisPlayer = player;
					distance = currentDist;
					
				}
				
				if(currentDist < distance){
					
					thisPlayer = player;
					distance = currentDist;
					
				}
				
			}
			
			return thisPlayer;
			
		}
		
		public static MyPlanet GetNearestPlanet(Vector3D position){
			
			MyPlanet planet = MyGamePruningStructure.GetClosestPlanet(position);
			
			return planet;
			
		}
		
		public static Vector3D GetRandomCompassDirection(Vector3D position, MyPlanet planet){
			
			if(planet == null){
				
				return Vector3D.Zero;
				
			}
			
			var planetEntity = planet as IMyEntity;
			var upDir = Vector3D.Normalize(position - planetEntity.GetPosition());
			var forwardDir = MyUtils.GetRandomPerpendicularVector(ref upDir);
			return Vector3D.Normalize(forwardDir);
			
		}
		
		public static double GetRandomPathDist(double minValue, double maxValue){
			
			return (double)rnd.Next((int)minValue, (int)maxValue);
			
		}
		
		public static bool IsPositionNearEntities(Vector3D coords, double distance){
			
			foreach(var entity in EntityList){
				
				if(entity as IMyCubeGrid == null && entity as IMyCharacter == null){
					
					continue;
					
				}
				
				if(Vector3D.Distance(coords, entity.GetPosition()) < distance){
					
					return true;
					
				}
				
			}
			
			return false;
			
		}
		
		public static bool IsPositionInGravity(Vector3D position, MyPlanet planet){
			
			if(planet == null){
				
				return false;
				
			}
			
			var planetEntity = planet as IMyEntity;
			var gravityProvider = planetEntity.Components.Get<MyGravityProviderComponent>();
			
			if(gravityProvider.IsPositionInRange(position) == true){
							
				return true;
				
			}
			
			return false;
			
		}
		
		public static bool IsPositionInSafeZone(Vector3D position){
			
			foreach(var safezone in safezoneList){
				
				var zoneEntity = safezone as IMyEntity;
				var checkPosition = position;
				bool inZone = false;
				
				if (safezone.Shape == MySafeZoneShape.Sphere){
					
					if(zoneEntity.PositionComp.WorldVolume.Contains(checkPosition) == ContainmentType.Contains){
						
						inZone = true;
						
					}
					
				}else{
					
					MyOrientedBoundingBoxD myOrientedBoundingBoxD = new MyOrientedBoundingBoxD(zoneEntity.PositionComp.LocalAABB, zoneEntity.PositionComp.WorldMatrix);
					inZone = myOrientedBoundingBoxD.Contains(ref checkPosition);
				
				}
				
				if(inZone == true){
					
					return true;
					
				}
				
			}
			
			return false;
			
		}
		
		public static bool IsPositionInTerritory(string territoryName, Vector3D position, double minDist = -1, double maxDist = -1){
			
			if(territoryName == ""){
				
				return false;
				
			}
			
			foreach(var territory in TerritoryManager.Territories){
				
				if(territory.Name != territoryName || territory.BadTerritory == true || territory.Active == false){
					
					continue;
					
				}
				
				if(territory.Type == "Static"){
					
					double distanceFromCenter = Vector3D.Distance(position, territory.Position);
					
					if(distanceFromCenter < territory.Radius){
						
						if(minDist > 0 && distanceFromCenter < minDist){
							
							continue;
							
						}
						
						if(maxDist > 0 && distanceFromCenter > maxDist){
							
							continue;
							
						}
						
						return true;
						
					}
					
				}
				
				if(territory.Type == "Planetary"){
					
					foreach(var planet in PlanetList){
						
						if(planet == null){
							
							continue;
							
						}
						
						var planetName = planet.Generator.Id.SubtypeId.ToString();
						
						if(territory.PlanetGeneratorName != planetName){
							
							continue;
							
						}
						
						var surfaceCoords = Vector3D.Zero;
						
						if(territory.Position != Vector3D.Zero){
							
							surfaceCoords = GetNearestSurfacePoint(territory.Position, planet);
							
						}else{
							
							var planetEntity = planet as IMyEntity;
							surfaceCoords = planetEntity.GetPosition();
							
						}
						
						var radius = territory.Radius;
						
						if(territory.ScaleRadiusWithPlanetSize == true){
							
							var unit = territory.Radius / 60000;
							radius = (double)planet.AverageRadius * unit;
							
							
						}
						
						double distanceFromCenter = Vector3D.Distance(position, surfaceCoords);
						
						if(distanceFromCenter < territory.Radius){
							
							if(minDist > 0 && distanceFromCenter < minDist){
								
								continue;
								
							}
							
							if(maxDist > 0 && distanceFromCenter > maxDist){
								
								continue;
								
							}
							
							return true;
							
						}
						
					}
					
				}
				
			}
			
			return false;
			
		}
		
		public static bool IsPositionNearEntity(Vector3D coords, double distance){
			
			foreach(var entity in EntityList){
				
				if(entity as IMyCubeGrid == null && entity as IMyCharacter == null){
					
					continue;
					
				}
				
				if(Vector3D.Distance(entity.GetPosition(), coords) < distance){
					
					return true;
					
				}
				
			}
			
			return false;
			
		}
		
		public static bool LunarSpawnEligible(Vector3D checkCoords){
			
			MyPlanet planet = GetNearestPlanet(checkCoords);
			
			if(planet == null){
				
				return false;
				
			}
			
			IMyEntity planetEntity = planet as IMyEntity;
			var upDir = Vector3D.Normalize(checkCoords - planetEntity.GetPosition());
			var closestPathPoint = upDir * Settings.SpaceCargoShips.MinLunarSpawnHeight + checkCoords;
			
			if(SpawnResources.IsPositionInGravity(closestPathPoint, planet) == true){
				
				return false;
				
			}
			
			return true;
			
		}
				
	}
	
}